package org.encyclosphere.encyclocrawler;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.encyclosphere.encyclocrawler.crawlers.Crawler;
import org.encyclosphere.encycloengine.api.zwi.Identity;
import org.encyclosphere.encycloengine.api.zwi.ZWIData;
import org.encyclosphere.encycloengine.api.zwi.ZWIDatabase;
import org.encyclosphere.encycloengine.util.Utilities;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import static org.encyclosphere.encyclocrawler.CrawlerUtils.getLogger;
import static org.encyclosphere.encyclocrawler.CrawlerUtils.getResourceAsString;

public class EncycloCrawler {

    public static final String PROJECT_VERSION = "2.0.0";

    public static String USER_AGENT = "EncycloCrawler (https://gitlab.com/ks_found/encyclocrawler)";

    /**
     * @hidden
     */
    public static Identity IDENTITY = new Identity();

    /**
     * @hidden
     */
    public static boolean SHOULD_SIGN;

    /**
     * @hidden
     */
    public static int TIME_BETWEEN_REQUESTS = 150;


    private static boolean crawlRunning;

    public static long ZWIS_GENERATED;

    private static final List<Crawler> CRAWLERS = Arrays.stream(getResourceAsString("/crawlers.txt").split("\n")).map(Crawler::loadCrawler).toList();

    public static boolean ENABLE_MEDIA_CACHE = true;

    /**
     * @hidden
     */
    public static ZWIDatabase DATABASE = null;

    /**
     * @hidden
     */
    public static void main(String[] args) throws Exception {
        setLoggingEnabled(true);

        // Print ASCII logo
        getLogger().info("███████╗███╗   ██╗ ██████╗██╗   ██╗ ██████╗██╗      ██████╗  ██████╗██████╗  █████╗ ██╗    ██╗██╗     ███████╗██████╗ ");
        getLogger().info("██╔════╝████╗  ██║██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔════╝██╔══██╗██╔══██╗██║    ██║██║     ██╔════╝██╔══██╗");
        getLogger().info("█████╗  ██╔██╗ ██║██║      ╚████╔╝ ██║     ██║     ██║   ██║██║     ██████╔╝███████║██║ █╗ ██║██║     █████╗  ██████╔╝");
        getLogger().info("██╔══╝  ██║╚██╗██║██║       ╚██╔╝  ██║     ██║     ██║   ██║██║     ██╔══██╗██╔══██║██║███╗██║██║     ██╔══╝  ██╔══██╗");
        getLogger().info("███████╗██║ ╚████║╚██████╗   ██║   ╚██████╗███████╗╚██████╔╝╚██████╗██║  ██║██║  ██║╚███╔███╔╝███████╗███████╗██║  ██║");
        getLogger().info("╚══════╝╚═╝  ╚═══╝ ╚═════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚══╝╚══╝ ╚══════╝╚══════╝╚═╝  ╚═╝ v" + PROJECT_VERSION);

        // Start crawling
        EncycloCrawlerConfig config = EncycloCrawlerConfig.load(Path.of("config.json"));
        Crawler crawler = getCrawler(config.getSelectedEncyclopedia());
        if(crawler != null) {
            getLogger().info("Crawling " + crawler.baseURL);
            long crawlStart = System.currentTimeMillis();
            long zwisGenerated = crawl(config);
            long crawlTime = (System.currentTimeMillis() - crawlStart) / 1000;
            long hours = crawlTime / 60 / 60 % 24;
            long minutes = crawlTime / 60 % 60;
            long seconds = crawlTime % 60;
            getLogger().info("Crawl complete! Generated " + NumberFormat.getInstance().format(zwisGenerated) + " ZWI files in " + hours + " hours, " + minutes + " minutes, and " + seconds + " seconds.");
        } else {
            throw new IllegalArgumentException("Crawler not found: \"" + config.getSelectedEncyclopedia() + "\"");
        }
    }

    /**
     * Crawls an encyclopedia.
     * @param config The {@link EncycloCrawlerConfig} to use
     * @return The number of ZWI files generated
     * @throws IllegalStateException If a crawl is already running
     * @throws Exception If a fatal error occurs during the crawl
     */
    public static long crawl(EncycloCrawlerConfig config) throws Exception {
        if(crawlRunning) throw new IllegalStateException("A crawl is already running");
        crawlRunning = true;

        // Delete the database if necessary
        if(config.shouldDeleteDatabaseOnStart()) {
            Utilities.deleteDirectory(config.getDatabaseRoot());
            Files.createDirectory(config.getDatabaseRoot());
        }

        // Create crawler data folders if they don't exist
        Files.createDirectories(Path.of("crawlerData/mediaCache"));

        // Get the crawler
        Crawler crawler = getCrawler(config.getSelectedEncyclopedia());
        if(crawler == null) throw new IllegalArgumentException("Crawler not found: \"" + config.getSelectedEncyclopedia() + "\"");

        DATABASE = new ZWIDatabase(config.getDatabaseRoot());
        SHOULD_SIGN = config.shouldSignFiles();
        IDENTITY = config.getIdentity();
        USER_AGENT = config.getUserAgent();
        TIME_BETWEEN_REQUESTS = config.getTimeBetweenRequests();

        // Create the config
        CrawlConfig crawlConfig = new CrawlConfig();
        crawlConfig.setCrawlStorageFolder(config.getCrawlerDataFolder().toAbsolutePath().toString());
        crawlConfig.setIncludeHttpsPages(true);
        crawlConfig.setPolitenessDelay(TIME_BETWEEN_REQUESTS);
        crawlConfig.setUserAgentString(USER_AGENT);
        crawlConfig.setResumableCrawling(config.isResumableCrawlingEnabled());
        crawlConfig.setMaxDownloadSize(Integer.MAX_VALUE);

        // Create the controller
        PageFetcher pageFetcher = new PageFetcher(crawlConfig);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false); // Allow crawling of any site
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(crawlConfig, pageFetcher, robotstxtServer);
        crawler.seedURLs.forEach(controller::addSeed);

        // Start the crawl
        controller.start(() -> crawler, 1);
        long generated = ZWIS_GENERATED;
        ZWIS_GENERATED = 0;
        return generated;
    }

    /**
     * Crawls an encyclopedia page and extracts a {@link ZWIData} object.
     * @param url The URL of the article
     * @return A {@link ZWIData} object
     */
    public static ZWIData crawl(String url) throws Exception {
        url = url.trim();
        for(Crawler crawler : CRAWLERS) {
            if(url.startsWith(crawler.baseURL)) {
                Document document = Jsoup.connect(url).userAgent(USER_AGENT).execute().parse();
                return crawler.crawl(url, document);
            }
        }
        throw new CrawlerNotFoundException("Unable to find a suitable crawler for URL: " + url);
    }

    /**
     * @param url The URL of an encyclopedia article
     * @return Whether EncycloCrawler can crawl it
     */
    public static boolean canCrawl(String url) {
        url = url.trim();
        for(Crawler crawler : CRAWLERS) {
            if(url.startsWith(crawler.baseURL)) return true;
        }
        return false;
    }

    /**
     * @hidden
     */
    public static Crawler getCrawler(String id) {
        for(Crawler crawler : CRAWLERS) {
            if(crawler != null && crawler.id.equals(id)) return crawler;
        }
        return null;
    }

    /**
     * Enables or disables the EncycloCrawler logger.
     * @param loggingEnabled Whether logging should be enabled
     */
    public static void setLoggingEnabled(boolean loggingEnabled) {
        if(loggingEnabled) getLogger().setLevel(Level.ALL);
        else getLogger().setLevel(Level.OFF);
    }

    public static class CrawlerNotFoundException extends Exception {
        public CrawlerNotFoundException(String message) {
            super(message);
        }
    }

}
