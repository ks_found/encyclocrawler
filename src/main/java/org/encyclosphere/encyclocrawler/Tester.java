package org.encyclosphere.encyclocrawler;

import net.lingala.zip4j.ZipFile;
import org.encyclosphere.encycloengine.api.zwi.Identity;
import org.encyclosphere.encycloengine.api.zwi.ZWIFile;
import org.encyclosphere.encycloengine.util.Utilities;
import org.jose4j.jwk.PublicJsonWebKey;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class Tester {

    public static void main(String[] args) throws Exception {
        EncycloCrawler.setLoggingEnabled(true);
        EncycloCrawler.USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0";//"EncycloCrawler (https://gitlab.com/ks_found/encyclocrawler)";
        PublicJsonWebKey key = PublicJsonWebKey.Factory.newPublicJwk(Files.readString(Path.of("keys.jwk")));
        Identity testIdentity = new Identity("EncycloSearch (Knowledge Standards Foundation", "USA", "did:psqr:encyclosearch.org#publish", key.getPublicKey(), key.getPrivateKey());
        Path zwiPath = Path.of("test.zwi");
        Files.deleteIfExists(zwiPath);
        Utilities.deleteDirectory(Path.of("test"));
        Objects.requireNonNull(EncycloCrawler.crawl("https://coloradoencyclopedia.org/article/base-and-industrial-metal-mining-colorado")).buildZWIFile(zwiPath).sign(testIdentity);
        ZipFile zwiZip = new ZipFile(new File("test.zwi"));
        zwiZip.extractAll("test");
        zwiZip.close();
        Identity identity = new ZWIFile(zwiPath).verify();
        System.out.println(identity.getName());
        System.out.println(identity.getAddress());
        System.out.println(identity.getPsqrKid());
    }

}
