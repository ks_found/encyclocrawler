package org.encyclosphere.encyclocrawler;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class CrawlerUtils {

    public static Logger logger = Logger.getLogger("EncycloCrawler");
    static {
        // Configure the logger
        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        //noinspection unused
        handler.setFormatter(new Formatter() {

            // From https://stackoverflow.com/a/53211725/5905216
            // ANSI escape codes
            public static final String ANSI_RESET = "\u001B[0m";
            public static final String ANSI_BLACK = "\u001B[30m";
            public static final String ANSI_RED = "\u001B[31m";
            public static final String ANSI_GREEN = "\u001B[32m";
            public static final String ANSI_YELLOW = "\u001B[33m";
            public static final String ANSI_BLUE = "\u001B[34m";
            public static final String ANSI_PURPLE = "\u001B[35m";
            public static final String ANSI_CYAN = "\u001B[36m";
            public static final String ANSI_WHITE = "\u001B[37m";


            @Override
            public String format(LogRecord record) {
                StringBuilder builder = new StringBuilder();

                if(record.getLevel() == Level.SEVERE) {
                    builder.append(ANSI_RED);
                } else if(record.getLevel() == Level.WARNING) {
                    builder.append(ANSI_YELLOW);
                } else {
                    builder.append(ANSI_WHITE);
                }

                builder.append("[");
                builder.append(calcDate(record.getMillis()));
                builder.append("]");

                builder.append(" [");
                builder.append(record.getLevel().getName());
                builder.append("]");

                builder.append(" ");
                builder.append(record.getMessage());

                Object[] params = record.getParameters();

                if (params != null) {
                    builder.append("\t");
                    for (int i = 0; i < params.length; i++) {
                        builder.append(params[i]);
                        if (i < params.length - 1)
                            builder.append(", ");
                    }
                }

                builder.append(ANSI_RESET);
                builder.append("\n");
                return builder.toString();
            }

            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            private String calcDate(long ms) {
                return dateFormat.format(new Date(ms));
            }
        });
        logger.addHandler(handler);

        logger.setLevel(Level.OFF);
    }

    public static byte[] getResource(String path) {
        try(InputStream resourceStream = CrawlerUtils.class.getResourceAsStream(path)) {
            if(resourceStream == null) throw new FileNotFoundException("Unable to locate resource: " + path);
            return resourceStream.readAllBytes();
        } catch(IOException e) {
            System.err.println("Unable to retrieve resource. Details:");
            e.printStackTrace();
        }
        return null;
    }

    public static String getResourceAsString(String path) {
        byte[] resourceBytes = getResource(path);
        assert resourceBytes != null;
        return new String(resourceBytes);
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void printStackTrace(Logger logger, Exception e) {
        printStackTrace(logger, e, Level.SEVERE);
    }

    public static void printStackTrace(Logger logger, Exception e, Level level) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        for(String line : sw.toString().split("\n")) logger.log(level, line);
    }

}
