package org.encyclosphere.encyclocrawler.crawlers;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MediaWikiCrawler extends StandardCrawler {

    public final String wikiPath;

    public final List<String> ignoredNamespaces;

    public MediaWikiCrawler(JSONObject definition) {
        super(definition);

        // Default values for MediaWiki
        if(!this.titleSelectorCustom) this.titleSelector = "#firstHeading";
        if(!this.contentSelectorCustom) this.contentSelector = "#mw-content-text";
        if(!this.toRemoveSelectorCustom) this.toRemoveSelector = ".printfooter, .mw-editsection, .nv-view, .nv-talk, .nv-edit";
        if(!this.isArticleCheckSelectorCustom) this.isArticleCheckSelector = ".mw-content-ltr";
        this.cssFiles.add("mediawiki.css");

        // Set MediaWiki-specific variables; populate the list of ignored URLs based on the list of ignored namespaces
        this.wikiPath = definition.has("wikiPath") ? definition.getString("wikiPath") : "/wiki/";
        this.ignoredNamespaces = new ArrayList<>(List.of(
                "Media:",
                "Special:",
                "Talk:",
                "User:",
                "User talk:",
                "File:",
                "File talk:",
                "MediaWiki:",
                "MediaWiki talk:",
                "Template:",
                "Template talk:",
                "Help:",
                "Help talk:",
                "Category:",
                "Category talk:"
        ));
        JSONArray ignoredNamespacesArray = definition.getJSONArray("ignoredNamespaces");
        for(int i = 0; i < ignoredNamespacesArray.length(); i++) {
            this.ignoredNamespaces.add(ignoredNamespacesArray.getString(i));
        }
        for(String ignoredNamespace : this.ignoredNamespaces) {
            this.ignoredURLs.add(this.baseURL + this.wikiPath + ignoredNamespace.replace(" ", "_"));
            if(ignoredNamespace.contains(" ")) this.ignoredURLs.add(this.baseURL + this.wikiPath + ignoredNamespace.replace(" ", "%20"));
        }
        this.requiredURLs.add(this.baseURL + this.wikiPath);
    }

    private boolean shouldConsider(WebURL url) {
        String urlStr = url.getURL().toLowerCase();
        return !(urlStr.contains("redirect=no") || urlStr.contains("oldid=") || urlStr.contains("action="));
    }

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        return super.shouldVisit(referringPage, url) && shouldConsider(url);
    }

    @Override
    public boolean shouldFollowLinksIn(WebURL url) {
        return super.shouldFollowLinksIn(url) && shouldConsider(url);
    }

}
