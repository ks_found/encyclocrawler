package org.encyclosphere.encyclocrawler.crawlers;

import io.github.crew102.rapidrake.RakeAlgorithm;
import io.github.crew102.rapidrake.data.SmartWords;
import io.github.crew102.rapidrake.model.RakeParams;
import io.github.crew102.rapidrake.model.Result;
import org.encyclosphere.encycloengine.api.zwi.ZWIData;
import org.encyclosphere.encycloengine.api.zwi.ZWIMetadata;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.TreeMap;

public class MetadataOnlyCrawler extends Crawler {

    // Code from https://github.com/crew102/rapidrake-java
    static RakeAlgorithm rake;
    static {
        // Create an object to hold algorithm parameters
        String[] stopWords = new SmartWords().getSmartWords();
        String[] stopPOS = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
        int minWordChar = 1;
        boolean shouldStem = true;
        String phraseDelims = "[-,.?():;\"!/]";
        RakeParams params = new RakeParams(stopWords, stopPOS, minWordChar, shouldStem, phraseDelims);

        // Create a RakeAlgorithm object
        String posTaggerURL = "/model-bin/en-pos-maxent.bin"; // The path to your POS tagging model
        String sentDetecURL = "/model-bin/en-sent.bin"; // The path to your sentence detection model
        try {
            String posTaggerPath = getTempFilePath(posTaggerURL, "posTagger");
            String sentDetecPath = getTempFilePath(sentDetecURL, "sentDetec");
            rake = new RakeAlgorithm(params, posTaggerPath, sentDetecPath);
        } catch(IOException e) {
            throw new RuntimeException("Unable to initialize rake algorithm", e);
        }
    }

    private static String getTempFilePath(String resourceLocation, String tempPrefix) throws IOException {
        byte[] bytes = Objects.requireNonNull(MetadataOnlyCrawler.class.getResourceAsStream(resourceLocation)).readAllBytes();
        Path tempFile = Files.createTempFile(tempPrefix, null);
        Files.write(tempFile, bytes);
        return tempFile.toAbsolutePath().toString();
    }

    public MetadataOnlyCrawler(JSONObject definition) {
        super(definition);
    }

    @Override
    public ZWIData crawl(
        String url,
        Document document,
        ZWIData data,
        ZWIMetadata metadata,
        Elements content
    ) {
        // Special cases
        specialCase("iep", () -> {
            boolean sawReferences = false;
            for(Element element : content.get(0).children()) {
                if(element.text().equals("Table of Contents")) element.remove();
                if(element.tagName().equals("h2") && element.text().contains("References and Further Reading")) sawReferences = true;
                if(sawReferences) element.remove();
            }
            metadata.setDescription(clean(metadata.getDescription()));
        });
        specialCase("okhistoryencyclopedia", () -> metadata.setTitle(metadata.getTitle().substring(0, metadata.getTitle().length() - 1))); // Remove period from end of title
        specialCase("connecticuthistory", () -> metadata.setTitle(
                metadata.getTitle().replace("_-_Connecticut_History_|_a_CTHumanities_Project", "").replace("_|_Connecticut_History_|_a_CTHumanities_Project", "")
        ));

        // Get significant phrases; order from most significant to least significant
        Result rakeResult = rake.rake(content.text()).distinct();
        String[] phrases = rakeResult.getFullKeywords();
        float[] scores = rakeResult.getScores();
        TreeMap<Float, String> sortedPhraseMap = new TreeMap<>();
        for(int i = 0; i < phrases.length; i++) {
            sortedPhraseMap.put(scores[i], clean(phrases[i]));
        }
        ArrayList<String> sortedPhrases = new ArrayList<>(sortedPhraseMap.values());
        Collections.reverse(sortedPhrases);
        metadata.getSignificantPhrases().addAll(sortedPhrases);

        // Return the result
        return data;
    }

    private String clean(String text) {
        return text.replace("“", "\"").replace("”", "\"").replace("‘", "'").replace("’", "'").trim();
    }

}
