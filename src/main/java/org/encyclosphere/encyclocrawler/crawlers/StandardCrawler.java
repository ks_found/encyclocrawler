package org.encyclosphere.encyclocrawler.crawlers;

import org.encyclosphere.encyclocrawler.EncycloCrawler;
import org.encyclosphere.encycloengine.api.zwi.ZWIContent;
import org.encyclosphere.encycloengine.api.zwi.ZWIData;
import org.encyclosphere.encycloengine.api.zwi.ZWIMetadata;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static org.encyclosphere.encyclocrawler.CrawlerUtils.*;
import static org.encyclosphere.encyclocrawler.EncycloCrawler.ENABLE_MEDIA_CACHE;
import static org.encyclosphere.encycloengine.util.Utilities.decodeURIComponent;
import static org.encyclosphere.encycloengine.util.Utilities.getSHA256Hash;

public class StandardCrawler extends Crawler {

    public StandardCrawler(JSONObject definition) {
        super(definition);
    }

    public static final Path MEDIA_CACHE = Path.of("crawlerData/mediaCache").toAbsolutePath();

    private static ConcurrentHashMap<String, byte[]> wheFiles;

    @Override
    public ZWIData crawl(String url,
                         Document document,
                         ZWIData data,
                         ZWIMetadata metadata,
                         Elements content) throws Exception {

        // Make all links absolute
        for(Element link : content.select("a")) link.attr("href", link.absUrl("href"));

        // Special cases for World History Encyclopedia
        specialCase("whe", () -> {
            Elements allElements = document.getAllElements();
            for(Element element : allElements) {
                if(element.hasAttr("data-src")) {
                    element.attr("src", element.attr("data-src"));
                    element.removeAttr("data-src");
                }
            }
            if(!document.select("#time_list").isEmpty()) {
                Element timeline = document.createElement("div");
                timeline.append("<h2>Timeline</h2>");
                Element timelineList = document.select("#time_list").first();
                for(Element timelineItem : timelineList.children())
                    timelineItem.attr("style", "padding-bottom: 20px");
                timeline.appendChild(timelineList);
                content.select("#tag_definition_wrapper").first().appendChild(timeline);
            }
            Elements reviewHeaders = document.select(".editorial_review_header");
            if(!reviewHeaders.isEmpty()) {
                Element reviewHeader = reviewHeaders.get(0);
                assert reviewHeader.parent() != null;
                reviewHeader.parent().prepend("<h3>" + reviewHeader.html() + "</h3>");
                reviewHeader.remove();
            }
            content.select(".figtitle").append("\n"); // Add newlines between the image caption and attribution, so they display on different lines
        });

        // Process images
        for(Element image : content.select("img")) {
            String src = image.absUrl("src");
            ZWIContent imageFile = getMediaFile(image, src, "image");          // Get the image
            if(imageFile == null)
                continue;                                    // If the image is null, it's in the list of ignored images, so continue
            image.attr("src", "data/media/images/" + imageFile.getFilename()); // Make the image src relative
            data.getImages().add(imageFile);                                   // Add the image to the ZWI file

            // If the image has a srcset, download all the images in the srcset
            if(image.hasAttr("srcset")) {
                getLogger().info("Image has a srcset. Processing images in srcset: " + src);
                String[] srcset = image.attr("srcset").split(", ");
                List<String> newSrcset = new ArrayList<>();
                List<ZWIContent> srcsetImages = new ArrayList<>();
                for(String fullURL : srcset) {
                    String[] split = fullURL.split(" ");
                    if(split.length != 2) {
                        getLogger().warning("Image has an invalid srcset: " + src);
                        image.removeAttr("srcset");
                        data.getImages().removeAll(srcsetImages);
                        break;
                    }
                    String scaledURL = split[0].trim(), scale = split[1].trim();
                    ZWIContent scaledImage = getMediaFile(null, processURL(scaledURL), "image");
                    if(scaledImage == null) continue;
                    newSrcset.add("data/media/images/" + scaledImage.getFilename() + " " + scale);
                    data.getImages().add(scaledImage);
                    srcsetImages.add(scaledImage);
                }
                image.attr("srcset", String.join(", ", newSrcset));
            }
        }

        // Process audio
        for(Element audio : content.select("audio")) {

            // Download the audio files.
            // An audio cache is not used because audio doesn't appear often,
            // and is rarely used more than once.
            for(Element source : audio.select("source")) {
                ZWIContent audioFile = getMediaFile(source, source.absUrl("src"), "audio");
                if(audioFile == null) {
                    source.remove();
                    continue;
                }
                data.getAudio().add(audioFile);
                source.attr("src", "data/media/audio/" + audioFile.getFilename());
            }

            // Download the subtitles.
            // A subtitle cache is not used for the same reason an audio cache is not used.
            for(Element track : audio.select("track")) {
                ZWIContent trackFile = getMediaFile(track, track.absUrl("src"), "track");
                if(trackFile == null) {
                    track.remove();
                    continue;
                }
                data.getOtherFiles().add(trackFile);
                track.attr("src", "data/other/" + trackFile.getFilename());
            }
        }

        // Process videos
        if(!this.id.equals("wikipedia")) { // TODO Temporary workaround for Wikipedia
            for(Element video : content.select("video")) {

                // Process the poster (thumbnail)
                if(video.hasAttr("poster")) {
                    ZWIContent thumbnailFile = getMediaFile(null, video.absUrl("poster"), "thumbnail");
                    if(thumbnailFile == null) {
                        video.removeAttr("poster");
                    } else {
                        data.getImages().add(thumbnailFile);
                        video.attr("poster", "data/media/images/" + thumbnailFile.getFilename());
                    }
                }

                // Download the video files.
                // A video cache is not used because videos don't appear often,
                // and are rarely used more than once.
                for(Element source : video.select("source")) {
                    ZWIContent videoFile = getMediaFile(source, source.absUrl("src"), "video");
                    if(videoFile == null) {
                        source.remove();
                        continue;
                    }
                    data.getVideos().add(videoFile);
                    source.attr("src", "data/media/videos/" + videoFile.getFilename());
                }

                // Download the subtitles.
                // A subtitle cache is not used for the same reason a video cache is not used.
                for(Element track : video.select("track")) {
                    ZWIContent trackFile = getMediaFile(track, track.absUrl("src"), "track");
                    if(trackFile == null) {
                        track.remove();
                        continue;
                    }
                    data.getOtherFiles().add(trackFile);
                    track.attr("src", "data/other/" + trackFile.getFilename());
                }
            }
        } else {
            for(Element video : content.select("video")) {
                if(video.hasAttr("poster")) video.attr("poster", video.absUrl("poster"));
                for(Element source : video.select("source")) source.attr("src", source.absUrl("src"));
                for(Element track : video.select("track")) track.attr("src", track.absUrl("src"));
            }
        }

        specialCase("whe", () -> {
            wheFiles = new ConcurrentHashMap<>();
            for(Element iframe : content.select("iframe")) {
                if(iframe.attr("src").startsWith("/template/files/map_animation.php")) {
                    try {
                        String mapDocContents = Jsoup.connect(iframe.absUrl("src")).userAgent(EncycloCrawler.USER_AGENT).execute().body();
                        Document mapDoc = Jsoup.parse(mapDocContents);
                        mapDoc.setBaseUri(this.baseURL);

                        // Get the map CSS
                        ZWIContent mapCSS = getWHEFile(mapDoc, iframe, "link", "href", "/template/css/final/ahe.css", "css", "whe-map.css");
                        if(mapCSS == null) return;
                        data.getCSSFiles().add(mapCSS);

                        // Get more map CSS
                        ZWIContent mapCSS2 = getWHEFile(mapDoc, iframe, "link", "href", "/template/css/exclude/map.css", "css", "whe-map-2.css");
                        if(mapCSS2 == null) return;
                        data.getCSSFiles().add(mapCSS2);

                        // Get the map JS
                        ZWIContent mapJS = getWHEFile(mapDoc, iframe, "script", "src", "/js/ahe-final.js", "other", "whe-map.js");
                        if(mapJS == null) return;
                        data.getOtherFiles().add(mapJS);

                        // Get jQuery
                        ZWIContent jQuery = getWHEFile(mapDoc, iframe, "script", "src", "https://ajax.googleapis.com/ajax/libs/jquery/", "other", "jquery.min.js");
                        if(jQuery == null) return;
                        data.getOtherFiles().add(jQuery);

                        // Get jCanvas
                        ZWIContent jCanvas = getWHEFile(mapDoc, iframe, "script", "src", "/js/jcanvas.min.js", "other", "jcanvas.min.js");
                        if(jCanvas == null) return;
                        data.getOtherFiles().add(jCanvas);

                        // Remove Cloudflare analytics
                        mapDoc.select("script[src^=\"https://static.cloudflareinsights.com/beacon.min.js\"]").remove();

                        // Get the HTML
                        ZWIContent mapHTML = new ZWIContent();
                        mapHTML.setFilename("whe-map.html");
                        mapHTML.setContent(mapDoc.outerHtml().getBytes());
                        data.getOtherFiles().add(mapHTML);
                        iframe.attr("src", "data/other/whe-map.html");
                    } catch(IOException e) {
                        getLogger().warning("Unable to load map: " + iframe.attr("src") + ". Details:");
                        printStackTrace(getLogger(), e, Level.WARNING);
                        iframe.attr("src", iframe.absUrl("src"));
                    }
                }
            }
        });

        // Add additional CSS
        for(String cssFile : this.cssFiles) {
            byte[] cssBytes = getResource("/css/" + cssFile);
            if(cssBytes == null) System.exit(1);
            ZWIContent css = new ZWIContent();
            css.setFilename(cssFile);
            css.setContent(cssBytes);
            data.getCSSFiles().add(css);
        }

        // Add article.html
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<!DOCTYPE html>\n");
        htmlBuilder.append("<html>\n");
        htmlBuilder.append("    <head>\n");
        htmlBuilder.append("        <meta charset=\"UTF-8\">\n");
        htmlBuilder.append("        <title>").append(metadata.getTitle().replace("_", " ")).append("</title>\n");
        for(String cssFile : this.cssFiles)
            htmlBuilder.append("        <link rel=\"stylesheet\" href=\"data/css/").append(cssFile).append("\">\n");
        htmlBuilder.append("    </head>\n");
        htmlBuilder.append("    <body>\n");
        for(String line : content.html().split("(\\r\\n|\\r|\\n)"))
            htmlBuilder.append("       ").append(line.replace("\t", "    ")).append("\n");
        htmlBuilder.append("    </body>\n");
        htmlBuilder.append("</html>\n");

        ZWIContent html = new ZWIContent();
        html.setFilename("article.html");
        html.setContent(htmlBuilder.toString().getBytes());
        data.getArticleFiles().add(html);

        // Add article.txt
        ZWIContent text = new ZWIContent();
        text.setFilename("article.txt");
        text.setContent(content.stream().map(Element::wholeText).collect(Collectors.joining("\n")).getBytes());

        // Return the result
        return data;
    }

    private ZWIContent getMediaFile(Element mediaElement, String src, String mediaType) throws IOException {

        // Make sure the media URL isn't on the list of ignored URLs
        for(String ignoredSrc : this.ignoredMediaURLs) {
            if(src.contains(ignoredSrc)) {
                getLogger().info("Skipping " + mediaType + ": " + src);
                if(mediaElement != null) mediaElement.remove();
                return null;
            }
        }

        // If it isn't, continue
        src = src.split("\\?")[0]; // Remove query parameters
        getLogger().info("Processing " + mediaType + ": " + src);
        ZWIContent mediaFile = new ZWIContent();
        String filename = getFilenameFromSrc(src);
        byte[] mediaBytes;

        // Check if the media is present in the cache.
        // If it is, retrieve it from there.
        Path mediaInCache = null;
        if(ENABLE_MEDIA_CACHE) {
            mediaInCache = MEDIA_CACHE.resolve(getSHA256Hash(src.getBytes()));
            if(Files.exists(mediaInCache)) {
                getLogger().info("Loading " + mediaType + " from cache: " + src);
                mediaBytes = Files.readAllBytes(mediaInCache);
                mediaFile.setFilename(filename);
                mediaFile.setContent(mediaBytes);
                getLogger().info("Loaded " + mediaType + " from cache: " + src);
                return mediaFile;
            }
        }

        // If it isn't, download it, and add it to the cache
        getLogger().info("Downloading " + mediaType + ": " + src);
        try {
            mediaBytes = Jsoup.connect(src)
                              .userAgent(EncycloCrawler.USER_AGENT)
                              .ignoreContentType(true)
                              .execute()
                              .bodyAsBytes();
            getLogger().info("Successfully downloaded " + mediaType + ": " + src);
            Thread.sleep(Math.max(0, EncycloCrawler.TIME_BETWEEN_REQUESTS - (System.currentTimeMillis() - lastRequestTime.get()))); // Ensures a delay between media requests
            lastRequestTime.set(System.currentTimeMillis());
            mediaFile.setFilename(filename);
            mediaFile.setContent(mediaBytes);
            if(ENABLE_MEDIA_CACHE) {
                assert mediaInCache != null;
                Files.write(mediaInCache, mediaBytes);
            }
        } catch(Exception e) {
            getLogger().warning("Failed to download " + mediaType + ": " + src);
            printStackTrace(getLogger(), e, Level.WARNING);
            mediaFile.setFilename("broken-" + mediaType + "-" + UUID.randomUUID());
            mediaFile.setContent(new byte[] {});
        }

        // Return the finished media file
        return mediaFile;
    }

    private boolean shouldRemoveMedia(Element media, String src) {
        boolean ignoreImage = false;
        for(String ignoredMediaURL : this.ignoredMediaURLs) {
            if(src.contains(ignoredMediaURL)) {
                media.remove();
                ignoreImage = true;
            }
        }
        return ignoreImage;
    }

    private String processURL(String url) {
        if(url.startsWith("//"))
            return "https://" + url.substring(2);
        else if(url.startsWith("/"))
            return this.baseURL + url;
        else if(url.startsWith("http://") || url.startsWith("https://"))
            return url;
        else
            return this.baseURL + "/" + url;
    }

    private String getFilenameFromSrc(String src) {
        if(src.contains("/media/math/render/svg")) src = src.trim() + ".svg"; // Add .svg to filename if it's a math file
        return decodeURIComponent(src.substring(src.lastIndexOf('/') + 1).split("\\?")[0]);
    }

    private ZWIContent getWHEFile(Document mapDoc,
                                  Element iframe,
                                  String element,
                                  String attribute,
                                  String startsWith,
                                  String folder,
                                  String filename) throws IOException {
        ZWIContent file = new ZWIContent();
        Element selectedElement = mapDoc.select(element + "[" + attribute + "^=\"" + startsWith + "\"]").first();
        if(selectedElement == null) {
            getLogger().warning("Unable to load map: " + iframe.attr("src"));
            iframe.attr("src", iframe.absUrl("src"));
            return null;
        }
        if(wheFiles.get(filename) == null) {
            wheFiles.put(filename, Jsoup.connect(selectedElement.absUrl(attribute)).userAgent(EncycloCrawler.USER_AGENT).ignoreContentType(true).execute().bodyAsBytes());
        }
        file.setFilename(filename);
        file.setContent(wheFiles.get(filename));
        selectedElement.attr("src", "data/" + folder + "/" + filename);
        return file;
    }

}
