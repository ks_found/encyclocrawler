package org.encyclosphere.encyclocrawler.crawlers;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.encyclosphere.encycloengine.api.zwi.ZWIData;
import org.encyclosphere.encycloengine.api.zwi.ZWIFile;
import org.encyclosphere.encycloengine.api.zwi.ZWIMetadata;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.encyclosphere.encyclocrawler.CrawlerUtils.*;
import static org.encyclosphere.encyclocrawler.EncycloCrawler.*;

public abstract class Crawler extends WebCrawler {

    public String id, type, license, baseURL;

    public List<String> seedURLs, ignoredURLs, requiredURLs, ignoredMediaURLs, cssFiles;

    public String titleSelector, contentSelector, disambiguatorSelector,
                  toRemoveSelector, isArticleCheckSelector, isNotArticleCheckSelector;

    public boolean titleSelectorCustom, contentSelectorCustom, disambiguatorSelectorCustom,
                   toRemoveSelectorCustom, isArticleCheckSelectorCustom, isNotArticleCheckSelectorCustom;

    public final boolean useTitleGetter, titleToTitleCase;

    public final AtomicLong lastRequestTime = new AtomicLong(System.currentTimeMillis());

    private static final List<String> STRING_VALUES = List.of(
            "id", "type", "license", "baseURL"
    );

    private static final List<String> ARRAY_VALUES = List.of(
            "ignoredURLs", "ignoredMediaURLs", "requiredURLs", "cssFiles"
    );

    private static final List<String> SELECTOR_STRING_VALUES = List.of(
            "titleSelector", "contentSelector", "disambiguatorSelector",
            "toRemoveSelector", "isArticleCheckSelector", "isNotArticleCheckSelector"
    );

    private static final HashMap<String, char[]> NOTATIONS = new HashMap<>();
    static {
        NOTATIONS.put("<a-z>", "abcdefghijklmnopqrstuvwxyz".toCharArray());
        NOTATIONS.put("<A-Z>", "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray());
        NOTATIONS.put("<a-z+0>", "abcdefghijklmnopqrstuvwxyz0".toCharArray());
        NOTATIONS.put("<A-Z+0>", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0".toCharArray());
        NOTATIONS.put("<a-z+0-9>", "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray());
        NOTATIONS.put("<A-Z+0-9>", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray());
    }

    public Crawler(JSONObject definition) {
        try {

            // Get strings
            for(String fieldName : STRING_VALUES) {
                Field field = Crawler.class.getDeclaredField(fieldName);
                field.set(this, definition.getString(fieldName));
            }

            // Get arrays
            for(String arrayName : ARRAY_VALUES) {
                Field arrayField = Crawler.class.getDeclaredField(arrayName);
                arrayField.set(this, new ArrayList<>());
                @SuppressWarnings("unchecked")
                ArrayList<String> array = (ArrayList<String>) arrayField.get(this);
                if(definition.has(arrayName)) {
                    JSONArray jsonArray = definition.getJSONArray(arrayName);
                    for(int i = 0; i < jsonArray.length(); i++)
                        array.add(jsonArray.getString(i));
                }
            }

            // Get selectors
            for(String fieldName : SELECTOR_STRING_VALUES) {
                if(definition.has(fieldName)) {
                    Field field = Crawler.class.getDeclaredField(fieldName);
                    field.set(this, definition.getString(fieldName));
                    Field isCustomField = Crawler.class.getDeclaredField(fieldName + "Custom");
                    isCustomField.set(this, true);
                }
            }
        } catch(NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            getLogger().severe("Error loading crawler. Details:");
            printStackTrace(getLogger(), e);
        }

        // Get misc additional properties
        this.seedURLs = new ArrayList<>();
        if(definition.has("seedURLs")) {
            JSONArray seedURLsArray = definition.getJSONArray("seedURLs");
            for(int i = 0; i < seedURLsArray.length(); i++) {
                String seedURL = seedURLsArray.getString(i)
                        .replace("<base>", this.baseURL);
                if(seedURL.startsWith("/")) seedURL = this.baseURL + seedURL; // Treat URLs with a leading slash as relative

                boolean notationMatched = false;
                for(Map.Entry<String, char[]> notation : NOTATIONS.entrySet()) {
                    if(seedURL.contains(notation.getKey())) {
                        notationMatched = true;
                        for(char letter : notation.getValue()) {
                            this.seedURLs.add(seedURL.replace(notation.getKey(), String.valueOf(letter)));
                        }
                    }
                }
                if(!notationMatched) this.seedURLs.add(seedURL);
            }
        }
        if(!(definition.has("excludeBaseURL") && definition.getBoolean("excludeBaseURL"))) {
            this.seedURLs.add(this.baseURL); // Automatically add the base URL to the list of seed URLs unless disabled
        }
        if(definition.has("excludeSeedURLs") && definition.getBoolean("excludeSeedURLs")) {
            this.ignoredURLs.addAll(this.seedURLs);
            this.ignoredURLs.remove(this.baseURL);
        }
        //if(this.seedURLs.isEmpty()) throw new IllegalStateException("No seed URLs given");
        this.useTitleGetter = definition.has("useTitleGetter") && definition.getBoolean("useTitleGetter");
        this.titleToTitleCase = definition.has("titleToTitleCase") && definition.getBoolean("titleToTitleCase");
    }

    /**
     * @hidden
     */
    public static Crawler loadCrawler(String id) {
        JSONObject definition = new JSONObject(getResourceAsString("/crawlers/" + id + ".json"));
        switch(definition.getString("type")) {
            case "standard" -> { return new StandardCrawler(definition); }
            case "mediaWiki" -> { return new MediaWikiCrawler(definition); }
            case "metadataOnly" -> { return new MetadataOnlyCrawler(definition); }
            default -> { return null; }
        }
    }

    @Override
    public void visit(Page page) {
        lastRequestTime.set(System.currentTimeMillis());
        String url = page.getWebURL().getURL();

        getLogger().info("Visiting URL: " + url);

        for (String ignoredURL : this.ignoredURLs) {
            if (url.contains(ignoredURL)) {
                getLogger().info("URL is on the list of ignored URLs. Not an article, skipping: " + url);
                return;
            }
        }

        boolean hasRequiredURL = this.requiredURLs.isEmpty();
        for (String requiredURL : this.requiredURLs) {
            if (url.contains(requiredURL)) {
                hasRequiredURL = true;
                break;
            }
        }
        if (!hasRequiredURL) {
            getLogger().info("URL is not on the list of required URLs. Not an article, skipping: " + url);
            return;
        }

        if (!(page.getParseData() instanceof HtmlParseData htmlParseData)) {
            getLogger().info("Page is not HTML. Not an article, skipping: " + url);
            return;
        }

        try {
            // Get the HTML
            String html = htmlParseData.getHtml();
            Document document = Jsoup.parse(html, this.baseURL);

            // Check if the page is an article
            boolean notAnArticle = false;

            // Use a selector to check if an element exists.
            // If the element doesn't exist, this isn't an article.
            //noinspection RedundantIfStatement
            if(this.isArticleCheckSelector != null && document.select(this.isArticleCheckSelector).isEmpty())
                notAnArticle = true;

            // Again, use a selector to check if an element exists.
            // This time, if the element exists, this isn't an article.
            if(this.isNotArticleCheckSelector != null && !document.select(this.isNotArticleCheckSelector).isEmpty())
                notAnArticle = true;

            // Skip page if it's not an article
            if(notAnArticle) {
                getLogger().info("Not an article, skipping: " + url);
                return;
            }

            // If the page is an article, build a ZWI file
            getLogger().info("Page is an article. Building ZWI file: " + url);
            ZWIData data = this.crawl(url, document);

            if (this.titleToTitleCase) {
                ZWIMetadata metadata = data.getMetadata();
                List<String> ignoredWords = List.of("the", "a", "an", "and", "but", "of", "or", "to", "in", "by", "with", "from", "for", "as", "if", "that", "which");
                metadata.setTitle(
                        Arrays.stream(metadata.getTitle().split("_"))
                                .map(s -> {
                                    if((s.contains(".") && !this.id.equals("okhistoryencyclopedia")) || (s.startsWith("(") && s.endsWith(")")) || s.equals("SIC")) return s;
                                    else if(ignoredWords.contains(s.toLowerCase())) return s.toLowerCase();
                                    else return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                                })
                                .collect(Collectors.joining("_"))
                );
            }

            ZWIFile zwiFile = data.buildZWIFile(DATABASE);
            if (this.useTitleGetter) {
                getLogger().info("Running title_getter...");
                ProcessBuilder builder = new ProcessBuilder("ruby", "title_getter.rb", zwiFile.getLocation().toAbsolutePath().toString()).directory(Path.of("desc_getter").toFile());
                Process process = builder.start();
                JSONObject info = new JSONObject(new String(process.getInputStream().readAllBytes()));
                if(!info.getBoolean("CorrectType")) {
                    getLogger().info("Not an article, deleting: " + url);
                    Files.delete(zwiFile.getLocation());
                    return;
                }
                ZWIMetadata metadata = data.getMetadata();
                if(!info.getString("ShortTitle").isEmpty()) metadata.setShortTitle(info.getString("ShortTitle"));
                metadata.setDescription(info.getString("Description"));
                JSONArray alternativeTitlesArray = info.getJSONArray("AlternativeTitles");
                for(int i = 0; i < alternativeTitlesArray.length(); i++) metadata.getAlternativeTitles().add(alternativeTitlesArray.getString(i));
                metadata.setStub(info.getBoolean("Stub"));
                metadata.setRedirect(info.getBoolean("Redirect"));
                metadata.setRedirectURL(info.getString("RedirectURL"));
                metadata.setCrossReference(info.getBoolean("CrossReference"));
                if(!info.isNull("CrossReferenceTitle")) metadata.setCrossReferenceTitle(info.getString("CrossReferenceTitle"));
                Files.delete(zwiFile.getLocation());
                data.buildZWIFile(DATABASE);
            }
            if(SHOULD_SIGN) zwiFile.sign(IDENTITY);
            ZWIS_GENERATED++;
            getLogger().info("Done. Saved to " + zwiFile.getLocation());
        } catch(Exception e) {
            getLogger().severe("Failed to build ZWI file. Details:");
            printStackTrace(getLogger(), e);
        }
    }

    public ZWIData crawl(String url, Document document) throws Exception {

        // Get the article content; remove unwanted elements
        Elements content = document.select(this.contentSelector);
        if(this.toRemoveSelector != null) content.select(this.toRemoveSelector).remove();

        // Create data and metadata; populate the metadata
        ZWIData data = new ZWIData();
        ZWIMetadata metadata = data.getMetadata();
        String title = document.select(this.titleSelector).text().replace(" ", "_");
        if(this.disambiguatorSelector == null) {
            metadata.setTitle(title);
        } else {
            Elements disambiguator = document.select(this.disambiguatorSelector);
            if(!disambiguator.isEmpty()) {
                String disambiguatorText = disambiguator.text();
                metadata.setDisambiguator(disambiguatorText);
                metadata.setShortTitle(title);
                metadata.setTitle(title + " (" + disambiguatorText + ")");
            }
        }
        metadata.setPublisher(this.id);
        metadata.setLastModified(LocalDateTime.now());
        metadata.setTimeCreated(LocalDateTime.now());
        metadata.setLicense(this.license);
        metadata.setSourceURL(url);
        metadata.setGeneratorName("EncycloCrawler (https://gitlab.com/ks_found/encyclocrawler)");

        // Special case for Encyclopedia Astronautica: Remove unwanted elements after processing the title, but before processing the description
        specialCase("encastronautica", () -> content.select("center:has(> a[href=\"../s/search.html\"]), font[size=\"-1\"]").remove());

        // Get the description
        Elements descriptionContent = content.select("p").clone();
        descriptionContent.select("sup").remove(); // Remove citations from description
        String description = descriptionContent.text();
        metadata.setDescription(description.length() < 350 ? description : description.substring(0, 350).trim());

        // Return the (un)finished data
        return this.crawl(url, document, data, metadata, content);
    }

    protected abstract ZWIData crawl(String url,
                                     Document document,
                                     ZWIData data,
                                     ZWIMetadata metadata,
                                     Elements content) throws Exception;

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String urlStr = url.getURL();
        /*for(String requiredURL : this.requiredURLs) {
            if(!urlStr.contains(requiredURL)) return false;
        }*/
        return urlStr.startsWith(this.baseURL);
    }

    @Override
    public boolean shouldFollowLinksIn(WebURL url) {
        return url.getURL().startsWith(this.baseURL);
    }

    public void specialCase(String id, Runnable toRun) {
        if(this.id.equals(id)) toRun.run();
    }

}
