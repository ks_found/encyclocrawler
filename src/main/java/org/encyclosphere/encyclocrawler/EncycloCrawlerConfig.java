package org.encyclosphere.encyclocrawler;

import org.encyclosphere.encycloengine.api.zwi.Identity;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class EncycloCrawlerConfig {

    private String selectedEncyclopedia, userAgent;

    private int timeBetweenRequests;

    private boolean resumableCrawling, deleteDatabaseOnStart, signFiles;

    private Path databaseRoot, crawlerDataFolder;

    private Identity identity;



    public static EncycloCrawlerConfig load(Path location) throws IOException, JoseException {
        JSONObject configObject = new JSONObject(Files.readString(location));
        JSONObject identity = configObject.getJSONObject("identity");
        EncycloCrawlerConfig.Builder builder = EncycloCrawlerConfig.builder()
                .encyclopedia(configObject.getString("selectedEncyclopedia"))
                .timeBetweenRequests(configObject.getInt("timeBetweenRequests"))
                .databaseRoot(Path.of(configObject.getString("databaseRoot")))
                .crawlerDataFolder(Path.of(configObject.getString("crawlerDataFolder")))
                .resumableCrawling(configObject.getBoolean("resumableCrawling"))
                .deleteDatabaseOnStart(configObject.getBoolean("deleteDatabaseOnStart"))
                .signFiles(configObject.getBoolean("signFiles"));
        if(configObject.has("userAgent")) builder.userAgent(configObject.getString("userAgent"));
        else builder.userAgent("EncycloCrawler (https://gitlab.com/ks_found/encyclocrawler)");
        if(configObject.getBoolean("signFiles")) {
            PublicJsonWebKey key = PublicJsonWebKey.Factory.newPublicJwk(Files.readString(Path.of("keys.jwk")));
            builder.identity(new Identity(identity.getString("name"), identity.getString("address"), identity.getString("psqrKid"), key.getPublicKey(), key.getPrivateKey()));
        }
        return builder.build();
    }

    public static Builder builder() {
        return new Builder();
    }



    public String getSelectedEncyclopedia() {
        return selectedEncyclopedia;
    }

    public void setSelectedEncyclopedia(String selectedEncyclopedia) {
        this.selectedEncyclopedia = selectedEncyclopedia;
    }

    public int getTimeBetweenRequests() {
        return timeBetweenRequests;
    }

    public void setTimeBetweenRequests(int timeBetweenRequests) {
        this.timeBetweenRequests = timeBetweenRequests;
    }

    public Path getDatabaseRoot() {
        return databaseRoot;
    }

    public void setDatabaseRoot(Path databaseRoot) {
        this.databaseRoot = databaseRoot;
    }

    public Path getCrawlerDataFolder() {
        return crawlerDataFolder;
    }

    public void setCrawlerDataFolder(Path crawlerDataFolder) {
        this.crawlerDataFolder = crawlerDataFolder;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public boolean isResumableCrawlingEnabled() {
        return resumableCrawling;
    }

    public void setResumableCrawling(boolean resumableCrawling) {
        this.resumableCrawling = resumableCrawling;
    }

    public boolean shouldDeleteDatabaseOnStart() {
        return deleteDatabaseOnStart;
    }

    public void setDeleteDatabaseOnStart(boolean deleteDatabaseOnStart) {
        this.deleteDatabaseOnStart = deleteDatabaseOnStart;
    }

    public boolean shouldSignFiles() {
        return signFiles;
    }

    public void setSignFiles(boolean signFiles) {
        this.signFiles = signFiles;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public static class Builder {

        private final EncycloCrawlerConfig config = new EncycloCrawlerConfig();

        public Builder encyclopedia(String encyclopedia) {
            config.setSelectedEncyclopedia(encyclopedia);
            return this;
        }

        public Builder timeBetweenRequests(int timeBetweenRequests) {
            config.setTimeBetweenRequests(timeBetweenRequests);
            return this;
        }

        public Builder databaseRoot(Path databaseRoot) {
            config.setDatabaseRoot(databaseRoot);
            return this;
        }

        public Builder crawlerDataFolder(Path crawlerDataFolder) {
            config.setCrawlerDataFolder(crawlerDataFolder);
            return this;
        }

        public Builder deleteDatabaseOnStart(boolean deleteDatabaseOnStart) {
            config.setDeleteDatabaseOnStart(deleteDatabaseOnStart);
            return this;
        }

        public Builder signFiles(boolean signFiles) {
            config.setSignFiles(signFiles);
            return this;
        }

        public Builder identity(Identity identity) {
            config.setIdentity(identity);
            return this;
        }

        public Builder userAgent(String userAgent) {
            config.setUserAgent(userAgent);
            return this;
        }

        public Builder resumableCrawling(boolean resumableCrawling) {
            config.setResumableCrawling(resumableCrawling);
            return this;
        }

        public EncycloCrawlerConfig build() {
            return config;
        }

    }

}
