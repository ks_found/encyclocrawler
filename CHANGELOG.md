# EncycloCrawler Changelog

## 2.0.0
Huge rewrite. Crawlers are now defined with JSON instead of being hard-coded in Java. Lots of new documentation.

## 1.1-1.2
Added some encyclopedias; code improvements

## 1.0
Initial release