# EncycloCrawler

A program that crawls encyclopedias, generating a database of [ZWI files](https://gitlab.com/ks_found/ZWISpec).

It's written in Java, and uses [Crawler4j](https://github.com/yasserg/crawler4j) to crawl websites.

## Supported encyclopedias
| Name | ID | Homepage | Metadata only? | # of ZWI files generated | Estimated crawling time (depends on Internet speed)
| --- | --- | --- | --- | --- | --- |
| Encyclopedia Britannica | britannica | https://britannica.com | Yes | 89267 | Unknown |
| Encyclopedia Mythica | encyclopediamythica | https://pantheon.org | No | 11405 | ~2h |
| Internet Encyclopedia of Philosophy | iep | https://iep.utm.edu | Yes | 841 | ~10m |
| International Standard Bible Encyclopedia | isbe | https://www.biblestudytools.com/encyclopedias/isbe | No | 9446 | ~1h 30m |
| New World Encyclopedia | nwe | https://www.newworldencyclopedia.org | No | ~18000 | Unknown |
| PCMag Encyclopedia | pcmag | https://www.pcmag.com/encyclopedia | Yes | 22318 | ~2h 50m |
| Stanford Encyclopedia of Philosophy | sep | https://plato.stanford.edu | Yes | 1768 | ~15m |
| Wikipedia | wikipedia | https://en.wikipedia.org | No | Unknown | Unknown |
| World History Encyclopedia | whe | https://worldhistory.org | No | 3195 | ~6h |

## Run it
Java 17+ required. Tested on Linux; may not work on macOS or Windows.

1. Open a terminal, and enter the following commands:
   ```
   git clone https://gitlab.com/ks_found/encyclocrawler.git
   cd encyclocrawler
   ```

2. Select an encyclopedia to crawl. Open `config.json` in your favorite text editor, and change `selectedEncyclopedia` to the ID of one of the encyclopedias above.

3. To start crawling, run the following command in the terminal from earlier:
   ```
   ./gradlew run
   ```

   Depending on the encyclopedia you select, crawling could take anywhere from several minutes to a few hours.
