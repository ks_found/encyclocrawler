# CURRENTLY TESTED ARTICLES:
TESTING = %w(
  https://encyclopediaofmath.org/wiki/Hill,_Austin_Bradford
)

# SOLVED ARTICLES:
SOLVED = %w(
  https://plato.stanford.edu/entries/archytas/
  https://plato.stanford.edu/entries/reasons-internal-external/
  https://plato.stanford.edu/entries/self-knowledge-externalism/
  https://ballotpedia.org/Nancy_Pelosi
  https://ballotpedia.org/Nancy_Fischman
  https://ballotpedia.org/Nancy_Kennedy_(Texas)
  https://pantheon.org/articles/z/zeus.html
  https://pantheon.org/articles/d/dius_fidius.html
  https://pantheon.org/articles/b/bagilat.html
  https://en.citizendium.org/wiki/Ant
  https://en.citizendium.org/wiki/Sunshine_(2007_film)
  https://en.citizendium.org/wiki/Larry_Sanger
  https://en.citizendium.org/wiki/Ren%C3%A9_Descartes
  https://edutechwiki.unige.ch/en/Wiki
  https://edutechwiki.unige.ch/en/Curriculum_planning
  https://edutechwiki.unige.ch/en/Instructional_curriculum_map
  https://edutechwiki.unige.ch/en/5e_Learning_cycle
  https://encyclopediaofmath.org/wiki/Elementary_number_theory
  https://encyclopediaofmath.org/wiki/Diophantine_equations
  https://encyclopediaofmath.org/wiki/Goryachev-Chaplygin_top
  https://eyewiki.org/Episcleritis
  https://eyewiki.org/Three_Step_Test_for_Cyclovertical_Muscle_Palsy
  https://handwiki.org/wiki/Engineering:List_of_Intel_Atom_microprocessors
  https://handwiki.org/wiki/Physics:Two-photon_physics
  https://handwiki.org/wiki/Wavelength
  https://handwiki.org/wiki/Biography:Albert_Einstein
  https://www.biblestudytools.com/encyclopedias/isbe/joseph-2.html
  https://www.biblestudytools.com/encyclopedias/isbe/joseph-barsabbas.html
  https://www.biblestudytools.com/encyclopedias/isbe/john-gospel-of.html
  https://www.biblestudytools.com/encyclopedias/isbe/creation.html
  https://iep.utm.edu/theatetu/
  https://iep.utm.edu/epistemo/
  https://iep.utm.edu/virtueep/
  http://www.scholarpedia.org/article/Tactile_Substitution_for_Vision
  http://www.scholarpedia.org/article/Corollary_discharge_in_primate_vision
  http://www.scholarpedia.org/article/Morris_water_maze
  http://www.scholarpedia.org/article/Insect_motion_vision
  https://en.wikipedia.org/wiki/Ant
  https://en.wikipedia.org/wiki/George_Malcher
  https://en.wikipedia.org/wiki/George_Rolfe
  https://en.wikipedia.org/wiki/George_Gadson
  https://en.wikipedia.org/wiki/Augmented_reality
  https://en.wikipedia.org/wiki/General_authority
  https://en.wikipedia.org/wiki/Baruch_Spinoza
  https://en.wikipedia.org/wiki/Newbridge,_Edinburgh
  https://en.wikipedia.org/wiki/Hetair-
  https://wikitia.com/wiki/Broda_Shaggi
  https://wikitia.com/wiki/Monce_C._Abraham
  https://wikitia.com/wiki/Real_Fruit_Bubble_Tea
  https://www.worldhistory.org/Hellenic_World/
  https://www.worldhistory.org/Genpei_War/
  https://www.worldhistory.org/Trojan_War/
  https://www.worldhistory.org/Zeno_of_Citium/
  https://encyclopediaofmath.org/wiki/Quadratically_closed_field
  https://encyclopediaofmath.org/wiki/Orthogonal_group
  https://encyclopediaofmath.org/wiki/Prime_number
)

# PROBLEMATIC ARTICLES:
UNSOLVED = %w(
  https://en.wikipedia.org/wiki/Hetair-
)

#Notes on unsolved:
=begin
  # I'd like to include the bullet points in the following. Not sure how.
  https://en.wikipedia.org/wiki/Hetair-
=end
