#!/usr/bin/env ruby

require 'strings'
require 'json'
require_relative 'urls'
require_relative 'article_fun'
include ArticleFun        # This is where all the heavy lifting is done.

#############################################################################
# FUNCTION CALLS: Actually execute stuff.
#############################################################################

# Declare key variables.
html = ''
url = ''
urls = []

# Decide whether (1) or (2).
html, url, urls = decide_script_mode(html, url, urls)

# (1) Execute only the inner method, which takes HTML & url from an argument.
#     Used when executed on command line with two args. Requested by Sergei.
unless url == ''                 # I.e., if there is one url.
  desc, article_text = make_description_from_html(html, url)
  zwidata = {
    Article: article_text,
    Description: desc
  }
  puts JSON.generate(zwidata)
end

# (2) Actually execute the enclosing method, which takes urls from urls.rb.
#     Used when executed WITHOUT args.
unless urls == []                # I.e., if there is a urls list.
  # Outputs desc and prints descriptions to terminal.
  descs = make_descriptions_from_urls(urls: urls, quiet: false)
  # Write output to desc_output.txt. Only for (2). Useful for testing.
  # This does not, but could, do anything with article_text.
  File.open("desc_output.txt", "w") do |f|
    descs.each do |desc_url|
      desc, url = desc_url
      f.puts
      f.puts Strings.wrap(desc, 78)
      f.puts url
    end
    puts "#{descs.count} descriptions and URLs saved to desc_output.txt."
  end
end
