#############################################################################
# MODULE
#############################################################################
# References:
# https://www.scrapingbee.com/blog/web-scraping-ruby/
# https://gist.github.com/carolineartz/10276637
# https://www.rubyguides.com/2015/06/ruby-regex/

require 'open-uri'
require 'nokogiri'
require 'strings'


module ArticleFun

#############################################################################
# Decider function. NOTE: this script can be used in two different ways.
# (1) Accept as CL arg, path to HTML file + source URL => desc.
# (2) If no argument passed, output descriptions from URLs in urls.rb.
def decide_script_mode(html, url, urls)
  # (1) Array of paths to html files (command line args).
  # To test, execute on command line:
  # > ruby desc_getter.rb article.html https://en.wikipedia.org/wiki/Proton
  # Or:
  # > ruby desc_getter.rb article.html wikipedia
  # This interprets passed arguments as a path to an HTML file.
  unless ARGV[0] == nil
    ARGV.each_with_index do |arg, i|
      if i == 0
         # Attempt to open file and put content in html.
        html = File.read(arg)
      elsif i == 1
        url = arg
      end
    end
  end

  # (2) Import URLs from urls.rb. Available lists are TESTING, SOLVED, and
  # UNSOLVED. The SOLVED list was producing good descriptions as of 2022/06/29.
  # In the following, get URLs from 'urls.rb' if no command line argument.
  urls = TESTING if html == ''

  return html, url, urls
end


#############################################################################
# Main function: Iterate over list of URLs and extract description.
# Takes a url list, outputs a hash where keys = urls and values = descriptions.
# NOTE: needs renaming. No longer just "urls" but "zwi folders" too.
def make_descriptions_from_urls(urls: [], html: '', quiet: false)
  desc_list = []                  # Will contain array of urls + descriptions.

  # Get html if passed. Strategy: if last item in urls *isn't* a URL (doesn't
  # start with "http") then it's the html. Assign it to html.
  html = urls.pop unless urls.last.match(/^http/)

  # Iterate over array of URLs.
  urls.each do |url|                     # Each loop extracts 1 desc per url.
    html = URI.open(url) unless html.nil?  # Download the article.
    desc, article_text = make_description_from_html(html, url)
    desc_list << [desc, url, article_text]
    unless quiet
      puts Strings.wrap(desc, 80)   # Output in terminal (= desc_output.txt)
      puts url
      puts
    end
  end
  return desc_list
end # of make_descriptions_from_urls

#############################################################################
# Most of the logic is here: given html and url, use Nokogiri to remove non-
# article stuff and massage the text until (up to) 350 chars of a
# description are outputted.
def make_description_from_html(html, url)
  doc = Nokogiri::HTML(html)    # Prepare DOM-type object containing article.

  # START Logic that strips out non-article stuff.

  # Ballotpedia special case.
  # Ballotpedia has bunches of crap in <p> near the top of the articles
  # that aren't actually part of the article.
  if url.include? 'ballotpedia'
    # Needs special selector to find first sentence of actual article.
    desc = doc.css('p')
    desc = desc -
           doc.css('style') -
           doc.css('.leg-hnt-flex-column') -
           doc.css('td p') -            # Any <p> in a td =/= article text.
           doc.css('.infobox') -         # Infobox outta here!
           doc.css('.widget-row p') -
           doc.css('div[style="text-align:center;"] p')
    # Ballotpedia erred, putting stylesheet data in plain text on the page.
    # This simply removes the style data by searching for one item.
    desc.children.each do |node|
      if node.content.match(/\w\-container\W/) ||
         node.content.match("leg-hnt-container") ||
         node.content.match("infobox") ||
         node.content.match("<pre>") ||
         node.content.match("does not contain the most recently published")
        node.content = ''
      end
    end

  elsif url.include? 'biblestudytools'  # ISBE special case.
    desc = doc.css('article')           # Conveniently uses HTML5 <article>!
    # ZWIBuilder strips out <article>, I guess.
    desc = doc.css('p') if desc.inner_html == ''
    # Fetch title in order to locate which (title) text to remove.
    # To avoid redundantly adding the title to the beginning of description,
    # title.length chars will be deleted from the start of the desc,
    # *except* in those cases where deleting multiple shortlines (below)
    # happens also to delete the title, as does happen.
    title = doc.css('title').text
               .gsub(" - International Standard Bible Encyclopedia", "")

  # The typical cases, which don't require much special handling.
  # The "special" items are in self-contained tags and thus easily deleted.
  # Note that the "-" is Nokogiri code to remove matching nodes from nodeset.
  else
    desc = doc.css('p') -               # Restrict all articles to <p> elements
           doc.css('td p') -            # Remove paras inside tables.
           doc.css('.noprint p') -      # Special for Edutech Wiki.
           doc.css('.onlyinprint p') -  # Ditto. Appears only in printing.
           doc.css('.cp-author-order p') - # Special for Scholarpedia.
           doc.css('p.card-text') -     # Special for Pantheon.org (EM).
           doc.css('p.eoa') -           # Ditto. End of article.
           doc.css('p.text-muted')      # Ditto. Date of publication.

    # Pantheon.org (EMyth). Removes notes; can't "subtract" this as above.
    desc.css('sup').remove if desc.css('sup')

    # The following deletes nodes based on matching text within the node.
    # Iterate through nodes looking for matching text; delete when spotted.
    # Text to match depends on source.

    # Scholarpedia.
    # Author lists at beginning of article need stripping out.
     if url.include? 'scholarpedia'
      desc.children.each do |node|
        if node.content.match("accepted the invitation on") or
           node.content.match("self-imposed deadline")
          node.content = ''
        end
      end

    # Wikitia.
    # Short articles can have non-article <p> content before end of 350 chars.
    elsif url.include? 'wikitia'
      desc.children.each do |node|
        if node.content.match("The list of its authors can be seen")
          node.content = ''
        end
      end

    # Encyclopedia of Mathematics.
    # Note that EOM we want to preserve LaTeX code so it can be rendered in
    # HTML properly, so more work might need to be done.
    elsif url.include? 'encyclopediaofmath'
      desc.children.each do |node|
        # Removes one-per-line items from before start of article.
        if node.content.match("Mathematics Subject Classification")
          node.parent.content = ''
        end
      end
    end # of big if-elsif logic that handles special cases.
  end # of giant if-elsif logic that strips out non-article stuff.

  # Add space after, so there is a space between paragraphs.
  desc.css('p').each do |node|
    # Add space after each Nokogiri::XML::Text.
    node.content = node.content + " \n"
  end

  # At this point, doc has been saved as desc, with extraneous, non-article
  # material all stripped out. It's still a Nokogiri object.

  # Finally, prep 350-char desc.
  # First, prepare a text blob from Nokogiri object.
  desc = desc                   # Extract description.
    .text                       # Extract text from object.

  # Certain manipulation has to be done to the string that couldn't be done
  # to the Nokogiri object.

  # Special case: ISBE often has tables of content that need removing.
  # Strategy: remove short lines (TOC). First, splits on <p>; then set title
  # to '' if it was removed along with the short lines (which happens).
  if url.include? 'biblestudytools'
    desc = remove_short_lines(desc, title) # See below.
    # If title is not in the first n chars of desc, set title to ''.
    # Otherwise, title.length chars will be removed from start of desc.
    title = '' if desc[0..title.length].strip.downcase != title.downcase
  end

  # ALL cases.
  # Massage text.
  desc = desc
    .gsub(" ", " ")        # Not sure where < 0xa0 > chars come from.
    .gsub(/\[\w+\]/, "")        # Strip ftnts.
    .strip                      # Remove leading and trailing whitespace.
    .gsub(/\s+/, " ")           # Remove repeated whitespace.

  # Special case: ISBE. Remove redundant title from beginning of ISBE desc
  # unless title has already been removed.
  if url.include? 'biblestudytools'
    desc = desc[title.length+1..-1] unless title.length == 0
  end

  # Create a plain text version of the file. Better than the ZWI text file?
  article_text = desc

  # Further massage text into newline-less string.
  desc = desc
    .gsub(/\n|\r/, " ")[0,350]  # Remove newlines and select 350 chars.

  # Delete final short sentence fragments.
  # If there is '.' or '?' or '!' within the last 40 characters, searching
  # from the end, remove from the last such character to the end of the string.
  # Strategy: make substring of last 40 chars, if total length is > 200 chars.
  # Find index of last punct char (. ? !) in that substring. If found, copy
  # from index + 1 to end (= to_delete). Remove that string from end using gsub.
  if desc.length > 200
    final_string = desc[-41..-1]
    last_period = last_q_mark = last_bang = last_colon = last_semic = 0
    # Find the last matching period-then-' '
    last_period = final_string.rindex(". ") if final_string.match('\. ')
    last_q_mark = final_string.rindex("? ") if final_string.match('\? ')
    last_bang   = final_string.rindex("! ") if final_string.match('\! ')
    last_colon  = final_string.rindex(": ") if final_string.match('\: ')
    last_semic  = final_string.rindex("; ") if final_string.match('\; ')
    # Get the index of the last period, etc.
    last = [last_period, last_q_mark, last_bang, last_colon, last_semic].max
    # Skip manipulation if not spotted in 40 chars.
    if last > 0
      to_delete = final_string[last+1..-1]
      # Delete to_delete from *end* of *desc* string (only).
      desc = desc.chomp(to_delete)
    end
  end

  return desc, article_text      # Return the finished description & article.
end # of make_description_from_html

#############################################################################
# Special logic for ISBE.
# Used to remove TOC from ISBE. Strategy: remove sequential short lines.
# Should work for any pedia that has a TOC in <p> tags without fancier markup.
def remove_short_lines(desc, title)
  desc_array = desc.split("\n")  # Make array for processing.
  dont_delete = []
  desc_array[0..2].each_with_index do |line, n|
    line = line.strip
    # Remove redundant title from first line.
    line = line.gsub(/^#{title}/i, '')
    next if line == nil
    # But don't delete first lines ending in ':'; also, don't delete lines
    # beginning "See " (these are cross-reference entries).
    if (line.match(/\:$/) or line.match(/^See /))
      dont_delete.push(n)
    end
  end
  # Construct new array of line lengths.
  desc_array_lengths = desc_array.map {|item| item.length}
  # Initialize array of any short lines.
  shortlines = []
  # Iterate array of lengths; push line to shortlines if under 75 chars.
  desc_array_lengths.each_with_index {|l, i| shortlines.push(i) if l < 75 }
  # Remove first pronunc./linguistic line and crossrefs from shortlines.
  if dont_delete.length > 0
    dont_delete.each do |n|
      shortlines.delete(n)
    end
  end
  # Sub-strategy: evaluate whether TOC spotted. How?
  # Basically: iterate through index numbers of short lines. You're looking
  # for series like [1, 2], and not [1, 3]. To spot these, you inquire whether
  # the last-seen number is one less than the current number. If so,
  # add the current index number to the list-to-delete (deletable).
  lastseen = 0
  deletable = []
  shortlines.each do |l|        # Note, l = index number of short line.
    # If this index is one more than the last
    if lastseen == l - 1
      # push the current line if the line with the index minus one was short.
      deletable.push(l)
    elsif l == 0                # 0 is a special. Add it if 1 is a shortline.
      deletable.push(l) if shortlines.find(1)
    end
    lastseen = l
  end

  # Delete all deletable. Delete from end to the beginning to avoid changing
  # indexes as you delete.
  deletable.reverse_each {|d| desc_array.delete_at(d)}
  # At this point, the passed desc is the same, minus any sequence of 2+
  # short lines, on the theory that any 2+ short lines is a TOC.
  desc_array.join(" ") # In Ruby, the last-seen value in a method is returned.
end # of remove_short_lines, for ISBE


##############################################################################
# TITLE-MASSAGING FUNCTIONS
##############################################################################

# Given a title, simply return the short title and the disambiguating phrase.
# If no disambiguating phrase, return the title and FALSE.
def get_disambiguator_from_parens(title)
  # Assumes all titles have only one set of parens.
  return ['', ''] unless title.match(/^(.+?)\((.+)\)$/)
  # Return [short title, disambiguating phrase] if possible
  $2 ? (return $1.strip, $2.strip) : (return title, "")
end

# Given title with comma, look for number after it (ISBE only for now).
def get_disambiguator_from_comma(title)
  return ['', ''] unless title.match(/^(.+?)\, (\d+)$/)
  return $1, $2
end

# Given a title and article, check for commas in title; if part to right is
# a numeral (from ISBE) then give special handling; otherwise, if part to
# right + " " + part to left can be found in the article, confirm and return
# an inverted title.
def massage_comma_titles(title, article)
  if title.match(/^(.+)?\, (.+?)$/)   # Try to match inverted title with comma.
    left = $1 if $1
    right = $2 if $2
    if right.match(/\d+/)             # Special case: number in rightmost part.
      return ""                       # Handled separately.
    elsif right                       # Check for inverted title.
      # Construct hypothesized inverted title.
      possible_title = right + " " + left
      # Remove "The" to catch some ISBE titles.
      possible_title2 = right.gsub("The ", "") + " " + left
      # Check for any match of possible_title in the article text.
      if article.match(/#{possible_title}/i)
        return possible_title         # Return found title.
      elsif article.match(/#{possible_title2}/i)
        return possible_title2
      else
        return ""                     # Hypothesized title not in text => "".
      end
    else                              # Can't do anything w/ this comma => "".
      return ""
    end
  else
    return ""                         # No ", " in title => "".
  end
end

# Given a title, return short title and topic.
# Used only on Handwiki. Adventurously assumes that all articles from Handwiki
# in the database are in fact encyclopedia articles and not articles from
# non-article namespaces.
def extract_topic_from_title(title)
  if title.match(/^(.+)?:(.+?)$/)
    return $2, $1
  else
    return ['', '']
  end
end

# Given a title, return any alt_titles.
# Used only on ISBE. If "Foo; Bar" then return ["Foo", "Bar"]; else return [].
def get_alt_titles_from_semicoloned_titles(title)
  # Strategy: search for one ';' and if spotted, split on them.
  if title.match(/^.+\;.+$/)
    # Since title contains at least one ';', split on ';', strip out any
    # whitespace, and return the array.
    alt_titles = title.split(';').each(&:strip!)
    return alt_titles
  else
    return []
  end
end

##############################################################################
# NON-ARTICLE MARKING FUNCTIONS
##############################################################################

# Extract redirect URL from html.
# I.e., given passed html that is assumed to contain a redirect URL, extract
# URL using Nokogiri tools, and return it. Used by check_for_redirect.
def extract_redirect_url(html)
  doc = Nokogiri::HTML(html)
  node = doc.css(".redirectText li a")
  redirect_url = node[0]['href']
  return redirect_url
end

# Given article, return redirect true/false, plus url if true.
def check_for_redirect(article, html)
  redirect = false
  if article.match(/^Redirect to/)
    redirect = true
    redirect_url = extract_redirect_url(html) # START HERE.
  end
  return redirect, redirect_url
end

# Given article, check if it starts with "See UPPERCASE". Return true for
# crossref, and return UPPERCASE (using Title Case) as crossref_title.
def check_for_crossrefs(article)
  # Assume any article longer than 250 chars is not a crossreference entry.
  return false if article.length > 250
  # Check first three lines that start with "See UPPERCASE".
  article.split("\n").each do |line|
    article.strip.match(/^See ([A-Z\d \,\;\:\'\(\)\-]{2,})/)
  end
  return false unless $1
  crossref_title = ($1 ? $1.gsub(/\w+/, &:capitalize) : "")
  return true, crossref_title
end

def expand_array_with_underscores(arr)
  bigger_arr = []
  arr.each do |item|
    if item.include? ' '
      bigger_arr << item.gsub(" ", "_")
    end
  end
  return arr + bigger_arr
end

# Given title, check for (for now, only Citizendium) non-article namespaces
# and subpages. If spotted, omit. Note, like other functions here, this could
# be expanded to cover other encyclopedias.
def discard_nonarticle_titles(title)
  cz_namespaces =
    ["Media", "Special", "main", "Talk", "User", "User talk", "Category",
      "Category talk", "CZ", "CZ Talk", "File", "File talk", "MediaWiki",
      "MediaWiki talk", "Template", "Template talk", "Help", "Help talk", "TI",
      "TI Talk", "WYA", "WYA Talk", "Citizendium Pilot",
      "Citizendium Pilot Talk", "Archive", "Archive talk"]
  cz_namespaces = expand_array_with_underscores(cz_namespaces)
  # For namespaces, match only when namespace is spotted, then ':', then
  # least some further characters.
  cz_namespaces.each do |ns|
    return false if title.match(/^#{ns}\:\w+/i)
  end

  # Random special pages that are in the main namespace, but not articles.
  delete_these = ["Main Page", "Main_Page", "Welcome_to_Citizendium",
                  "Welcome to Citizendium"]
  delete_these.each do |delt|
    return false if title.match(/^#{delt}$/i)
  end

  # For Citizendium subpages, match when "#" is followed by the subpage name.
  cz_subpages =
    ["Related Articles", "Definition", "Bibliography", "External Links",
      "Works", "Discography", "Filmography", "Catalogs", "Timelines", "Gallery",
      "Audio", "Video", "Code", "Tutorials", "Student Level", "Advanced",
      "Debate Guide", "Addendum", "Signed Articles", "Function", "Recipes",
      "News Guide"]
  cz_subpages = expand_array_with_underscores(cz_subpages)
  cz_subpages.each do |sp|
    return false if title.match(/\##{sp}$/i)
  end

  # If passed all tests, it's a true article...hopefully.
  return true
end

# Detect disambiguation (disam) pages.
# Used especially on Wikipedia-style wiki encyclopedias, uses <li> tags to
# determine whether most of the content of the page is wrapped by them
# (i.e., when surrounded by <ul>). Do character count of the *internal text*
# of the <li> tags vs. of the whole page. If > 75% of text is in such tags,
# return "true" (is disam). Otherwise "false". NOTE: this algorithm does not
# work for EdutechWiki, because they (nonstandardly) use bullet points for
# large amounts of content.
def detect_disambiguation_page(html, pub)
  return false if pub == "edutechwiki"
  # Convert html string to Nokogiri object for ease of processing.
  doc = Nokogiri::HTML(html)
  # Get all text on page that is inside either <p> or <li>, and do char count.
  p_char_count = doc.css('p').text.length           # MOVE THESE BACK DOWN!
  li_char_count = doc.css('ul li').text.length

  # Useful for testing disambig algorithm:
  # puts "p_char_count: #{p_char_count}"
  # puts "li_char_count: #{li_char_count}"
  # puts "<li> percentage: #{"%.1f%%" %
  #   ((li_char_count.to_f / (li_char_count.to_f + p_char_count.to_f)) * 100)}"

  # Search all href attributes; if any includes "Disambiguation", return true.
  doc.xpath('//@href').each do |href|
    return true if href.inner_html.include? "Disambiguation"
  end
  # No <p> but positive <li>?
  if p_char_count == 0
    (li_char_count > 0) ? (return true) : (return false)
  end
  # If 75% or more of the characters on the page that are inside <p> *or* <li>
  # tags are inside <li> tags, then it's probably (certainly?) a disam page.
  # While all pages marked by this are disam pages, some disam pages might not
  # be caught. 50% might be better.
  if (li_char_count / (li_char_count.to_f + p_char_count.to_f)) > 0.75
    return true
  else
    return false
  end
end


###
end # of module ArticleFun
