# title_getter and desc_getter

These are simple scripts for converting HTML from a small set of encyclopedias
to a 350-character (at most) description. This could be called from another
script or you could translate it to Java or Python.

title_getter takes a ZWI file or a directory (either the default or one you
specify) and outputs a bunch of info in JSON format. As of July 8, 2022, this
has been tested on about a dozen encyclopedias, and outputs a JSON data string
with the following information:

* HTML file encoding
* Word count
* The original source URL (for matching info)
* A rewritten description, up to 350 words
* The original title (for reference)
* A short title (removing Handwiki namespaces and Wikipedia disambiguators)
* Inverted title (for the ISBE, for now)
* Topics (taken from Handwiki namespaces)
* Alternative titles (for now, from ISBE titles with semicolons)
* Whether an article is a "stub" (defined as under 350 characters)
* Whether a page is a Wikipedia redirect page
* If so, what URL the page redirects to (so that the redirect page's title
can be added to its target's list of alternative titles)
* Whether an entry (from ISBE) is a cross-reference entry.
* If a cross-reference entry, the title of the article pointed to.
* Whether the page is in fact an article (for now, only rules out Citizendium
pages from non-article namespaces, as well as subpages).
* Whether the page is a "disambiguation" page, which has pointers to other
articles.

desc_getter simply returns a good description. This is the hardest task of the
above. This script is called by title_getter, so you really need only to use
the latter, although I suppose desc_getter would be faster if a description is
all you needed.

## Purpose

The purpose of these scripts is not to exhaustively improve the data of the
encyclosphere in every case, but simply to improve the current (summer 2022)
database. If we keep using any version of this, it will have to be maintained
regularly as we include new sources and as new problems come to light.

## Testing hint

If you want to test output, you'll probably want to comment out =begin and
=end (closer to the end of title_getter.rb), then comment out the code that
prints the JSON strings to the command line (which aren't exactly human-
readable), at the bottom of title_getter.rb (the three lines
`puts JSON.generate(zwidata)`), with: #

When you're done testing and want to revert to JSON-only output, simply remove
the `#` marks you added, which will comment out the human-readable output and
instead output a JSON string.

## To run title_getter and desc_getter
(1) Ensure you have installed the required gems:

* `gem install open-uri`
* `gem install nokogiri`
* `gem install strings`
* `gem install rubyzip`
* `gem install zip`
* `gem install json`
* `gem install words_counted`

(2) Run in one of the five following ways.

### Calling title_getter from command line with a single zwi file.
You must have a ZWI file and a path to it. You execute:

`ruby title_getter.rb <path to ZWI file>`

This outputs a JSON string for just that file to the command line.

### Calling title_getter from command line with a path to a directory.
You must have a directory containing one or more ZWI files, and a path to it.
You execute:

`ruby title_getter.rb <path to ZWI directory>`

This outputs a more complex JSON object, basically an array of the first sort,
one for each ZWI file.

### Calling title_getter from command line without arguments.
If you simply run

`ruby title_getter.rb`

then you will get a JSON object with data about whatever files happen to be in
the `./zwis` directory. This is good for testing (but you might want to change
what is shown on the command line; on which, see above).

### Calling desc_getter from command line with arguments.
You must have an HTML file and a path to it, as well as the URL of the source
of the file. From these items you will receive the description on the command
line:

`ruby desc_getter.rb <path to HTML file> <url>`

This order is required. You can but do not have to put the arguments `<path>`
and `<url>` in quotation marks.

This returns to the command line the description only.

### Calling from command line without arguments.
If no arguments are passed, then you can simply run

`ruby desc_getter.rb`

and then the script will (last we checked) create descriptions for all the URLs
listed in the `urls.rb` script's TESTING constant. You can change the constant
that the scripts gets input from by (1) in article_fun.rb, locating the line
`urls = TESTING if html == ''` (but this might be
`urls = <SOMETHING_ELSE> if html == ''`), (2) opening urls.rb
and looking for the constants, which are in ALLCAPS (there are three available:
SOLVED, UNSOLVED, and TESTING), and then either (3a) changing the URLs listed
in TESTING in urls.rb or (3b) changing the constant the script loads in
article_fun.rb. Note, at present we have no way to run title_getter.rb on URLs
for the simple reason that that script presupposes the existence of ZWI files
and is actually helping Sergei and Henry update the title and other data in
them.
