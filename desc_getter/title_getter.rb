#!/usr/bin/ruby
require 'zip'
require 'json'
require 'words_counted'
require_relative 'article_fun'
include ArticleFun        # Used to fetch a description and the article text.


##############################################################################
# CALL TITLE-AND-STUB-MASSAGING FUNCTIONS
##############################################################################

def process_zwi_file(zwi)
  json_text = ''
  html = ''
  zwidata = []
  Zip::File.open("#{zwi}") do |zipfile|
    zipfile.each do |subfile|
      # Unzip ZWI; extract metadata.json and html.
      # If the file (in the zip file) is metadata.json, read to json_text.
      json_text = subfile.get_input_stream.read if
        subfile.name == "metadata.json"
      html = subfile.get_input_stream.read if
        subfile.name == "article.html"
    end
    # MAIN ENCLOSING METHOD, CALLED HERE.
    zwidata = get_title_etc(html, json_text)
  end
  # Actually make a JSON object out of all the ZWI files.
  return zwidata
end

# Main enclosing method.
# Does both preparatory & function calling. Run once for each zwi/article.
def get_title_etc(html, json_text)

  #########################
  # PREPARATORY

  # Just a note: html is passed here and is available.
  html = html

  # Parse json.
  md = JSON.parse(json_text)

  # Get title and url from meta.
  title = md["Title"].gsub("_", " ")
  url = md["SourceURL"]
  pub = md["Publisher"]

  # Download article
  desc, article = make_description_from_html(html, url)

  # Count words
  counter = WordsCounted.count(title + article)
  word_count = counter.token_count

  # Detect encoding
  encoding = html.encoding

  ###############################
  # DECLARE VARIABLES to (possibly) fill in.
  short_title = disambiguator = topic = inverted_title = redirect_url =
                crossref_title = ''
  alt_titles = []
  correct_type = true
  redirect = stub = crossref = disam = false

  ###############################
  # CALL TITLE METHODS.

  # Disambiguator logic. Check ALL.
  short_title, disambiguator = get_disambiguator_from_parens(title)

  # Check for "Foo Bar, 1" disambiguators. Check ISBE.
  if pub == 'isbe'
    short_title, disambiguator = get_disambiguator_from_comma(title)
  end

  # Inverted title logic.
  # In titles of the form "Foo, Bar", see if "Bar Foo" has any matches in the
  # article text. If so, add "Bar Foo" to AlternativeTitles. Check ISBE.
  if pub == 'isbe'            #ISBE.
    inverted_title = massage_comma_titles(title, article)
  end

  # Handwiki topic logic.
  # Converts "Topic:Foo Bar" titles into short title and topic. Check handwiki.
  if pub == 'handwiki'
    short_title, topic = extract_topic_from_title(title)
  end

  # ISBE semicoloned alternative title logic.
  # Converts "Foo; Bar" and "Foo; Bar; Baz" titles from ISBE into two (or
  # three) alternative titles. Check ISBE.
  if pub == 'isbe'
    alt_titles = get_alt_titles_from_semicoloned_titles(title)
  end

  ###############################
  # CALL NON-ARTICLE METHODS
  # Marks stubs, redirects, cross-references, main pages, and other
  # non-article pages.

  # Mark stubs as such. Obviously, this could be ignored.
  stub = true if article.length < 350

  # Redirect page logic.
  # Identify Wikipedia redirect pages and handle appropriately. No other
  # redirect pages are caught; not sure there are redirect pages from any
  # other source in the database.
  if pub == 'wikipedia'          # Wikipedia only for now. Could be repurposed.
    redirect, redirect_url = check_for_redirect(article, html)
  end

  # Cross-reference logic.
  # For ISBE, identify cross-referenced entries and add to crossref.
  if pub == 'isbe'
    crossref, crossref_title = check_for_crossrefs(article)
  end

  # Eliminate Citizendium non-article namespaces as well as subpages. Sets
  # correct_type to false when non-article is spotted.
  if pub == 'citizendium'
    correct_type = discard_nonarticle_titles(title)
  end

  # Disambiguation (disam) page logic.
  # Identifies and marks disam pages on Wikipedia, Citizendium, Wikitia,
  # Handwiki, etc. All tested. Returns "true" if disam.
  disam = detect_disambiguation_page(html, pub)
  # BUT if a page is not an article, it shouldn't also be counted as a disam.
  # Ensures that if namespace, subpage, or redirect, it's NOT a disam.
  disam = false unless (correct_type && ! redirect)

=begin Comment-out when testing
  puts "=========="
  puts "#{title.upcase}\n#{article[0..100]}#{article.length > 300 ?
                                             "..." + article[-100..-1] : ''}"
  puts "ENCODING: #{encoding}"
  puts "WORD COUNT: #{word_count}"
  puts "URL: #{url}"
  puts "DESC:\n#{desc}"
  puts "ORIGINAL TITLE: #{title}"
  puts "     -----"
  puts "SHORT TITLE: #{short_title}" if short_title.length > 0
  puts "DISAMBIGUATOR: #{disambiguator}" if disambiguator.length > 0
  puts "INVERTED TITLE: #{inverted_title}" if inverted_title.length > 0
  puts "TOPIC: #{topic}" if topic.length > 0
  puts "ALTERNATIVE TITLES: #{alt_titles}" if alt_titles.length > 0
  puts "ARTICLE STUB STATUS: #{stub}" if stub
  puts "REDIRECT: #{redirect}" if redirect
  puts "REDIRECT URL: #{redirect_url}" if redirect
  puts "CROSS-REFERENCE: #{crossref}" if crossref
  puts "CROSS-REFERENCE TITLE: #{crossref_title}" if
                    (crossref_title != nil and crossref_title.length > 0)
  puts "CORRECT TYPE: #{correct_type}" unless correct_type
  puts "DISAMBIGUATION PAGE: #{disam}" if disam
  puts
  puts
=end

  zwidata = {
    Article: article,
    Encoding: encoding,
    WordCount: word_count,
    URL: url,
    Description: desc,
    OriginalTitle: title,
    ShortTitle: short_title,
    Disambiguator: disambiguator,
    InvertedTitle: inverted_title,
    Topic: topic,
    AlternativeTitles: alt_titles,
    Stub: stub,
    Redirect: redirect,
    RedirectURL: redirect_url,
    CrossReference: crossref,
    CrossReferenceTitle: crossref_title,
    CorrectType: correct_type,
    DisambiguationPage: disam
  }

  return zwidata
end

#############################################################################
# SCRIPT ACTUALLY STARTS HERE.

# (1) If param is passed on command line, interpret as ZWI file or as a
# directory of ZWI files. Process it/them.
if ARGV[0]
  arg = ARGV[0]

  # If it's a single ZWI file, treat it as such.
  if arg.match(/\w+\.zwi$/)
    zwidata = {}      # Will be a single hash.
    zwi = arg
    zwidata = process_zwi_file(zwi) # Takes a string (file path).
    puts JSON.generate(zwidata)

  # Otherwise assume it's a directory, and iterate over it.
  else
    zwidata = []          # Will be an array of hashes.
    zwifolder = ARGV[0]   # Assume it's a directory.
    zwifolder = zwifolder[0..-2] if zwifolder[-1] == '/'
    zwis = Dir["#{zwifolder}/*zwi"]
    zwis.each do |zwi|
      zwidata << process_zwi_file(zwi)
    end
    puts JSON.generate(zwidata)
  end

## (2) Iterate over *.zwi files in /zwis folder. Process all.
else
  zwidata = []      # Will be an array of hashes.
  zwis = Dir["./zwis/*zwi"]
  zwis.each do |zwi|
    next unless zwi.match(/.+\.zwi$/)
    zwidata << process_zwi_file(zwi)
  end
  puts JSON.generate(zwidata)
end
